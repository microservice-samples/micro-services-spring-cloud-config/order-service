package com.hendisantika.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * Project : Bank Account Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/21
 * Time: 08.29
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BankAccount implements Serializable {

    private String accountId;

    private String accountName;

    private AccountType accountType;

    private BigDecimal accountBalance;
}

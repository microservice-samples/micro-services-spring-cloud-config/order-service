package com.hendisantika.orderservice.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : order-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 17.13
 */
@Data
public class Payment {
    private int paymentId;
    private String paymentStatus;
    private String txId;
    private int orderId;
    private double amount;
}

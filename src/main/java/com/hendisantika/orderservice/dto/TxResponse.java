package com.hendisantika.orderservice.dto;

import com.hendisantika.orderservice.entity.Order;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : order-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 17.15
 */
@Data
public class TxResponse {
    private Order order;
    private double amount;
    private String txId;
    private String status;
}

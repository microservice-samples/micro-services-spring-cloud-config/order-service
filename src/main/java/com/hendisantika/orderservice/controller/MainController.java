package com.hendisantika.orderservice.controller;

import com.hendisantika.orderservice.dto.BankAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : order-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/12/21
 * Time: 14.24
 */
@RefreshScope
@RestController
public class MainController {
    @Autowired
    RestTemplate restTemplate;
    @Value("${text.copyright: Default Copyright}")
    private String copyright;
    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String userName;
    @Value("${spring.datasource.password}")
    private String password;
    @Autowired
    private DataSource dataSource;

    @GetMapping("/showConfig")
    @ResponseBody
    public String showConfig() {
        String configInfo = "Copy Right: " + copyright //
                + "<br/>spring.datasource.driverClassName=" + driverClassName //
                + "<br/>spring.datasource.url=" + url //
                + "<br/>spring.datasource.username=" + userName //
                + "<br/>spring.datasource.password=" + password;

        return configInfo;
    }

    @GetMapping("/pingDataSource")
    @ResponseBody
    public String pingDataSource() {
        try {
            return this.dataSource.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }
    }


    @GetMapping("/accounts")
    @ResponseBody
    public String getListBankAccount() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange("http://localhost:8081/bank-account", HttpMethod.GET, entity, String.class).getBody();
    }

    @GetMapping("/accounts/{id}")
    @ResponseBody
    public String getBankAccountById(@PathVariable("id") String id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange("http://localhost:8081/bank-account/" + id, HttpMethod.GET, entity,
                String.class).getBody();
    }

    @PostMapping(value = "/accounts")
    public String createBankAccount(@RequestBody BankAccount bankAccount) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<BankAccount> entity = new HttpEntity<>(bankAccount, headers);

        return restTemplate.exchange(
                "http://localhost:8080/bank-account", HttpMethod.POST, entity, String.class).getBody();
    }

    @GetMapping("/accounts2")
    @ResponseBody
    public BankAccount[] getListBankAccount2() {
        String url = "http://localhost:8081/bank-account";
        return restTemplate.getForObject(url, BankAccount[].class);
    }

    @GetMapping("/accounts2/{id}")
    @ResponseBody
    public ResponseEntity<BankAccount> getBankAccountById2(@PathVariable("id") String id) {
        String url = "http://localhost:8081/bank-account/";
        ResponseEntity<BankAccount> responseEntity;
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        return responseEntity = restTemplate.getForEntity(url + id, BankAccount.class, params);
    }
}

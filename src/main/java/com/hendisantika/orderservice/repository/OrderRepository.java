package com.hendisantika.orderservice.repository;

import com.hendisantika.orderservice.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : order-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 17.22
 */
public interface OrderRepository extends JpaRepository<Order, Integer> {
}

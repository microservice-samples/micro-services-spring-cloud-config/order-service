package com.hendisantika.orderservice.service;

import com.hendisantika.orderservice.dto.Payment;
import com.hendisantika.orderservice.dto.TxResponse;
import com.hendisantika.orderservice.entity.Order;
import com.hendisantika.orderservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by IntelliJ IDEA.
 * Project : order-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 17.24
 */
@Service
public class OrderService {
    @Autowired
    private OrderRepository repository;

    @Autowired
    private RestTemplate template;

    public TxResponse placeOrder(Order order) {
        repository.save(order);
        Payment paymentReq = new Payment();
        paymentReq.setOrderId(order.getId());
        paymentReq.setAmount(order.getQuantity() * order.getPrice());
        Payment paymentRes =
                template.postForObject("http://localhost:8082/payment/doPay/",
                        paymentReq, Payment.class);
        TxResponse txResponse = new TxResponse();
        txResponse.setOrder(order);
        txResponse.setStatus(paymentRes.getPaymentStatus());
        txResponse.setAmount(paymentRes.getAmount());
        txResponse.setTxId(paymentRes.getTxId());
        return txResponse;
    }
}

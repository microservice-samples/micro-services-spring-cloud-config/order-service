# Order Service

### Things todo list:

1. Clone this
   repository: `git clone https://gitlab.com/microservice-samples/micro-services-spring-cloud-config/order-service.git`
2. Navigate to the folder: `cd order-service`
3. Run the application: `mvn clean spring-boot:run`
4. Open POSTMAN APP:

Add New Order

```json
POST localhost:8081/order/placeOrder

REQUEST 
{
"id":1,
"name":"Headset",
"quantity":10,
"price":7500
}


RESPONSE
{
"order": {
"id": 1,
"name": "Headset",
"quantity": 10,
"price": 7500.0
},
"amount": 0.0,
"txId": "7e7396b7-9b94-42af-a5a6-71e3e024994c",
"status": "failure"
}
```
